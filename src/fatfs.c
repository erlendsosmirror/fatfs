/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "ff.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "print/print.h"
#include "posix/errno.h"
#include "posix/unistd.h"
#include "posix/fcntl.h"
#include "mem/string.h"
#include "mem/mem.h"
#include "kmod/kmod.h"
#include "sched/scheduler.h"
#include "syscall/svc-basicfileops.h"
#include "config.h"

///////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////
typedef struct FilesysTable
{
	FIL fp;
	int inuse;
} FilesysTable;

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
char vfs_fatfs_driverinitialized[FF_VOLUMES] = {0};
const struct DeviceDriver *vfs_fatfs_drivers[FF_VOLUMES] = {0};
struct FileNode *vfs_fatfs_nodes[FF_VOLUMES] = {0};
FATFS vfs_fatfs_disk[FF_VOLUMES];

_DIR directory;
FILINFO fno;

#if VFS_FATFS_DYNAMIC_MEM == 0
FilesysTable filtab[VFS_FATFS_NSTATIC];
#endif

///////////////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////////////
int TranslateMode (int m)
{
	int mode = FA_OPEN_EXISTING;

	if ((m & 0x0F) == 0)
		mode |= FA_READ;

	if (m & O_WRONLY)
		mode |= FA_WRITE;

	if (m & O_RDWR)
		mode |= FA_READ | FA_WRITE;

	if (m & O_APPEND)
		mode |= FA_OPEN_APPEND;
	else if (m & O_TRUNC)
		mode |= FA_CREATE_ALWAYS;
	else if (m & O_CREAT)
		mode |= FA_CREATE_NEW;

	return mode; // FA_READ | FA_WRITE | FA_CREATE_ALWAYS;
}


FIL *vfs_fatfs_alloc (void)
{
#if VFS_FATFS_DYNAMIC_MEM
	return (FIL*) MemAlloc (sizeof (FIL), 0);
#else
	for (int i = 0; i < VFS_FATFS_NSTATIC; i++)
	{
		if (!filtab[i].inuse)
		{
			filtab[i].inuse = 1;
			return &filtab[i].fp;
		}
	}

	return 0;
#endif
}

void vfs_fatfs_free (FIL *fp)
{
#if VFS_FATFS_DYNAMIC_MEM
	MemFree ((unsigned int) fp);
#else
	for (int i = 0; i < VFS_FATFS_NSTATIC; i++)
		if (&filtab[i].fp == fp)
			filtab[i].inuse = 0;
#endif
}

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int vfs_fatfs_open (struct FileNode *node, const char *filename, int flags, int mode)
{
	FIL *fp = vfs_fatfs_alloc ();

	if (!fp)
	{
		Print ("No more file descriptors\n");
		return -ENOMEM;
	}

	int ret = f_open (fp, filename, TranslateMode(node->flags));

	if (ret != FR_OK)
	{
		// TODO: Convert to errno values
		Print ("Fatfs error: %i\n", ret);
		vfs_fatfs_free (fp);
		return -ret;
	}

	node->priv = fp;
	return 0;
}

int vfs_fatfs_close (struct FileNode *node)
{
	int ret = f_close ((FIL*)node->priv);

	if (ret != FR_OK)
		return -ret;

	vfs_fatfs_free ((FIL*)node->priv);
	return 0;
}

int vfs_fatfs_read (struct FileNode *node, void *data, int nbytes)
{
	unsigned int bread;

	int ret = f_read (node->priv, data, nbytes, &bread);

	if (ret != FR_OK)
		return -ret;

	return bread;
}

int vfs_fatfs_write (struct FileNode *node, void *data, int nbytes)
{
	unsigned int bwritten;

	int ret = f_write (node->priv, data, nbytes, &bwritten);

	if (ret != FR_OK)
	{
		Print ("Fatfs error: %i\n", ret);
		return -ret;
	}

	return bwritten;
}

int vfs_fatfs_lseek (struct FileNode *node, int offset, int whence)
{
#if VFS_FATFS_ENABLE_LSEEK
	int ret = -1;

	if (whence == SEEK_END)
	{
		ret = f_size ((FIL*)node->priv);
		f_lseek ((FIL*)node->priv, ret);
		ret = f_tell ((FIL*)node->priv);
	}
	else if (whence == SEEK_SET)
	{
		ret = f_lseek ((FIL*)node->priv, offset);

		if (ret != FR_OK)
			ret = -1;

		ret = f_tell ((FIL*)node->priv);
	}

	return ret;
#else
	return -1;
#endif
}

int vfs_fatfs_fstat (struct FileNode *fnode, struct stat *st)
{
	// Not supported at the moment
	return -1;
}

int vfs_fatfs_stat (const char *filename, struct stat *st)
{
#if VFS_FATFS_ENABLE_STAT
	// Stat the file
	FILINFO fno;
	int ret = f_stat (filename, &fno);

	// Check
	if (ret != FR_OK)
		return -ret;

	// Set the appropriate flags
	if (fno.fattrib & AM_DIR)
		st->st_mode = S_IFDIR;
	else
		st->st_mode = S_IFREG;

	// Set the size
	st->st_size = fno.fsize;
	return 0;
#else
	return -1;
#endif
}

int vfs_fatfs_link (const char *oldname, const char *newname)
{
	// Not supported, use rename
	return -1;
}

int vfs_fatfs_unlink (const char *filename)
{
#if VFS_FATFS_ENABLE_UNLINK
	// Warning: rmdir needs an additional _MAX_SS * 2 stack space!!!
	int ret = f_unlink (filename);

	if (ret == FR_OK)
		return 0;
	return -1;
#else
	return -1;
#endif
}

int vfs_fatfs_rename (const char *oldname, const char *newname)
{
#if VFS_FATFS_ENABLE_RENAME
	int ret = f_rename (oldname, newname);

	if (ret == FR_OK)
		return 0;

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_mkdir (const char *name, int mode)
{
#if VFS_FATFS_ENABLE_MKDIR
	int ret = f_mkdir (name);

	if (ret == FR_OK)
		return 0;

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_opendir (const char *name)
{
#if VFS_FATFS_ENABLE_OPENDIR
	int ret = f_opendir (&directory, name);

	if (ret == FR_OK)
		return 0;

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_closedir (void)
{
#if VFS_FATFS_ENABLE_CLOSEDIR
	int ret = f_closedir (&directory);

	if (ret == FR_OK)
		return 0;

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_readdir (struct dirent *de)
{
#if VFS_FATFS_ENABLE_READDIR
	int ret = f_readdir (&directory, &fno);

	if (ret == FR_OK)
	{
		// Error if no name
		if (!fno.fname[0])
			return -1;

		// Copy up to 64 characters
		for (int i = 0; i < 64; i++)
		{
			de->d_name[i] = fno.fname[i];
			if (!fno.fname[i])
				break;
		}

		de->d_name[63] = 0;
		return 0;
	}

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_statvfs (const char *path, struct statvfs *st)
{
#if VFS_FATFS_ENABLE_STATVFS
	unsigned int free;
	int ret;
	FATFS *fs;

	if ((ret = f_getfree (path, (DWORD*) &free, &fs)) == FR_OK)
	{
		MemSet (st, 0, sizeof (struct statvfs));

		unsigned int tot_sect = (fs->n_fatent - 2) * fs->csize;
		unsigned int fre_sect = free * fs->csize;

		st->f_bsize = fs->ssize;
		st->f_frsize = fs->ssize;
		st->f_blocks = tot_sect;
		st->f_bfree = fre_sect;
		st->f_bavail = fre_sect;

		return 0;
	}

	return -ret;
#else
	return -1;
#endif
}

int vfs_fatfs_mount (struct FileNode *fnode, char *drvpath)
{
	for (int i = 0; i < FF_VOLUMES; i++)
	{
		if (!vfs_fatfs_driverinitialized[i])
		{
			vfs_fatfs_driverinitialized[i] = 1;
			vfs_fatfs_nodes[i] = fnode;
			vfs_fatfs_drivers[i] = fnode->dev;

			drvpath[0] = '0' + i;
			drvpath[1] = ':';
			drvpath[2] = '/';
			drvpath[3] = 0;

			if (f_mount (&vfs_fatfs_disk[i], drvpath, 0) != FR_OK)
				Print ("f_mount error\n");

			drvpath[2] = 0;

			return 0;
		}
	}

	return -1;
}

int vfs_fatfs_unmount (void)
{
	return -1;
}

///////////////////////////////////////////////////////////////////////////////
// Driver struct
///////////////////////////////////////////////////////////////////////////////
const struct VFSDriver vfs_driver_fatfs =
{
	.open = vfs_fatfs_open,
	.close = vfs_fatfs_close,
	.read = vfs_fatfs_read,
	.write = vfs_fatfs_write,
	.lseek = vfs_fatfs_lseek,
	.fstat = vfs_fatfs_fstat,
	.stat = vfs_fatfs_stat,
	.link = vfs_fatfs_link,
	.unlink = vfs_fatfs_unlink,
	.rename = vfs_fatfs_rename,
	.mkdir = vfs_fatfs_mkdir,
	.opendir = vfs_fatfs_opendir,
	.closedir = vfs_fatfs_closedir,
	.readdir = vfs_fatfs_readdir,
	.statvfs = vfs_fatfs_statvfs,
	.mount = vfs_fatfs_mount,
	.unmount = vfs_fatfs_unmount
};
