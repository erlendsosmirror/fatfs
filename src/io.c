/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "ff.h"
#include "diskio.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "ioctl/ioctl.h"
#include "print/print.h"
#include "config.h"

///////////////////////////////////////////////////////////////////////////////
// Externs
///////////////////////////////////////////////////////////////////////////////
extern const struct DeviceDriver *vfs_fatfs_drivers[FF_VOLUMES];
extern struct FileNode *vfs_fatfs_nodes[FF_VOLUMES];
extern FATFS vfs_fatfs_disk[FF_VOLUMES];

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
DSTATUS disk_status (BYTE pdrv)
{
	return RES_OK;
}

DSTATUS disk_initialize (BYTE pdrv)
{
	return RES_OK;
}

DRESULT disk_read (BYTE pdrv, BYTE *buff, DWORD sector, UINT count)
{
	// Optional debug message
#if VFS_FATFS_DEBUG_READ
	Print ("Read sect %i count %i\n", sector, count);
#endif

	// Seek to correct location
	vfs_fatfs_drivers[pdrv]->ioctl (vfs_fatfs_nodes[pdrv], SPIFSETSECT, (void*)sector);

	// Calculate number of bytes
	int nbytes = vfs_fatfs_disk[pdrv].ssize * count;

	// Read
	int ret = vfs_fatfs_drivers[pdrv]->read (vfs_fatfs_nodes[pdrv], buff, nbytes);

	// Check
	return (ret == nbytes) ? RES_OK : RES_ERROR;
}

DRESULT disk_write (BYTE pdrv, const BYTE *buff, DWORD sector, UINT count)
{
	// Optional debug message
#if VFS_FATFS_DEBUG_WRITE
	Print ("Write  sect %i count %i\n", sector, count);
#endif

	// Seek to correct location
	vfs_fatfs_drivers[pdrv]->ioctl (vfs_fatfs_nodes[pdrv], SPIFSETSECT, (void*)sector);

	// Calculate number of bytes
	int nbytes = vfs_fatfs_disk[pdrv].ssize * count;

	// Write
	int ret = vfs_fatfs_drivers[pdrv]->write(vfs_fatfs_nodes[pdrv], (void*)buff, nbytes);

	// Check
	return (ret == nbytes) ? RES_OK : RES_ERROR;
}

DRESULT disk_ioctl (BYTE pdrv, BYTE cmd, void *buff)
{
	switch (cmd)
	{
	case CTRL_SYNC:
		return 0;
	case GET_SECTOR_COUNT:
		return vfs_fatfs_drivers[pdrv]->ioctl(vfs_fatfs_nodes[pdrv], SPIFGETSECTCNT, buff);
	case GET_SECTOR_SIZE:
		return vfs_fatfs_drivers[pdrv]->ioctl(vfs_fatfs_nodes[pdrv], SPIFGETSECTSIZE, buff);
	case GET_BLOCK_SIZE:
		*(unsigned int*)buff = 1;
		return 0;
	default:
		return vfs_fatfs_drivers[pdrv]->ioctl(vfs_fatfs_nodes[pdrv], cmd, buff);
	}
}

DWORD get_fattime (void)
{
	return 0;
}
