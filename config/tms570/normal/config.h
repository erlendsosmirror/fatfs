/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// Enable (1) or disable (0) calls
#define VFS_FATFS_ENABLE_LSEEK 1
#define VFS_FATFS_ENABLE_STAT 1
#define VFS_FATFS_ENABLE_UNLINK 1
#define VFS_FATFS_ENABLE_RENAME 1
#define VFS_FATFS_ENABLE_MKDIR 1
#define VFS_FATFS_ENABLE_OPENDIR 1
#define VFS_FATFS_ENABLE_CLOSEDIR 1
#define VFS_FATFS_ENABLE_READDIR 1
#define VFS_FATFS_ENABLE_STATVFS 1

// Reformat mode (1), upload mode (2), reformat and upload (3) or normal mode (0)
// Note: Use make mkpreload if using option 2 or 3
#define VFS_FATFS_ENABLE_REFORMAT 0

// Enable (1) or disable (0) debug print messages
#define VFS_FATFS_DEBUG_CHIPCAP 1
#define VFS_FATFS_DEBUG_READ 0
#define VFS_FATFS_DEBUG_WRITE 0

// Number of allocated file objects
#define VFS_FATFS_NSTATIC 2

// Enable (1) or disable (0) dynamic memory allocation
#define VFS_FATFS_DYNAMIC_MEM 1

// Set to 1 if memory card is to be mounted
#define VFS_FATFS_HAVE_MEM_CARD 0
