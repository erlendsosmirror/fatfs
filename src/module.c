/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "ff.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "print/print.h"
#include "mem/string.h"
#include "kmod/kmod.h"
#include "sched/scheduler.h"
#include "syscall/svc-basicfileops.h"
#include "ioctl/ioctl.h"
#include "config.h"

///////////////////////////////////////////////////////////////////////////////
// Used functions
///////////////////////////////////////////////////////////////////////////////
int vfs_fatfs_statvfs (const char *path, struct statvfs *st);

///////////////////////////////////////////////////////////////////////////////
// Driver struct
///////////////////////////////////////////////////////////////////////////////
extern const struct VFSDriver vfs_driver_fatfs;

///////////////////////////////////////////////////////////////////////////////
// Standard module
///////////////////////////////////////////////////////////////////////////////
void FatfsInit (void)
{
	// Open the device
	int fakeerrno = 0;
	const char openfile[] = "/dev/spinor0";
	int openargs[4] = {(int)openfile, 0, 0, (int) &fakeerrno};
	int ret = svc_open (currentthc, openargs);

	if (ret < 0)
		Print ("Failed to open spi flash\n");

	// Get the jedec id, should be 0x001540EF for 25Q16
#if VFS_FATFS_DEBUG_CHIPCAP
	int ioctlargs[4] = {(int)ret, SPIFJEDEC, 0, (int) &fakeerrno};
	int jedec = svc_ioctl_fcntl (currentthc, ioctlargs);

	Print ("Got JEDEC ID 0x%x errno %i\n", jedec, fakeerrno);
	Print ("Capacity %i bytes\n", 1 << (jedec >> 16));
#endif

	// Get the file node
	unsigned char c = currentthc->pctrl->fnodes[ret];
	struct FileNode *f = &nodetable[c];

	// Mount it
	vfs_mount ("/", (struct VFSDriver*) &vfs_driver_fatfs, f);

	// Mount memory card if we have it
#if VFS_FATFS_HAVE_MEM_CARD
	const char openfilesd[] = "/dev/sdio";
	int openargssd[4] = {(int)openfilesd, 0, 0, (int) &fakeerrno};
	ret = svc_open (currentthc, openargssd);

	if (ret < 0)
		Print ("Failed to open sdio\n");

	c = currentthc->pctrl->fnodes[ret];
	f = &nodetable[c];

	vfs_mount ("/sd", (struct VFSDriver*) &vfs_driver_fatfs, f);
#endif
}

#if VFS_FATFS_ENABLE_REFORMAT == 0
KMOD_DESCRIPTOR (fatfs, FatfsInit, "FatFS");
#endif

///////////////////////////////////////////////////////////////////////////////
// Reformat module
///////////////////////////////////////////////////////////////////////////////
void PrintFileSystemStats (int ret, struct statvfs *st)
{
	Print ("ret %i\n", ret);
	Print ("st->f_bsize %i\n", st->f_bsize);
	Print ("st->f_frsize %i\n", st->f_frsize);
	Print ("st->f_blocks %i\n", st->f_blocks);
	Print ("st->f_bfree %i\n", st->f_bfree);
	Print ("st->f_bavail %i\n", st->f_bavail);
}

#if VFS_FATFS_ENABLE_REFORMAT == 1 || VFS_FATFS_ENABLE_REFORMAT == 3
unsigned char formatworkbuffer[4096];

int ReformatExternalFlash (void)
{
	Print ("\nStarting format procedure\n");

	int ok = f_mkfs ("", FM_ANY, 0, formatworkbuffer, sizeof(formatworkbuffer));

	Print ("Format result %i\n", ok);
	return ok;
}

#if VFS_FATFS_ENABLE_REFORMAT == 1
void FatfsInitReformat (void)
{
	FatfsInit ();

	struct statvfs st;
	MemSet (&st, 0, sizeof (st));

	int ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	ReformatExternalFlash ();

	ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);
}

KMOD_DESCRIPTOR (fatfs, FatfsInitReformat, "FatFS");
#endif
#endif

///////////////////////////////////////////////////////////////////////////////
// Upload module
///////////////////////////////////////////////////////////////////////////////
#if VFS_FATFS_ENABLE_REFORMAT == 2 || VFS_FATFS_ENABLE_REFORMAT == 3
FIL file1;
extern const unsigned char _binary_sh_elf_start;
extern const unsigned char _binary_sh_elf_end;
extern const unsigned char _binary_save_elf_start;
extern const unsigned char _binary_save_elf_end;

int UploadHelper (const char *path, const unsigned char *source, unsigned int nbytes)
{
	Print ("Writing %i bytes starting addr 0x%x\n", nbytes, (unsigned int) source);

	int ret = f_open (&file1, path, FA_READ | FA_WRITE | FA_CREATE_ALWAYS);

	if (ret != FR_OK)
		return ret;

	unsigned int bwritten;

	ret = f_write (&file1, source, nbytes, &bwritten);

	f_close (&file1);

	if (ret != FR_OK)
		return ret;

	if (bwritten != nbytes)
	{
		Print ("Incorrect number of bytes written\n");
		Print ("Expected %i got %i\n", nbytes, bwritten);
		return -1;
	}

	return 0;
}

void UploadExternalFlash (void)
{
	Print ("Starting write procedure\n");

	int ret = f_mkdir ("/bin");

	if (ret)
	{
		Print ("mkdif failed with code &i\n", ret);
		while (1);
	}

	unsigned int size = (unsigned int) &_binary_sh_elf_end - (unsigned int) &_binary_sh_elf_start;

	ret = UploadHelper ("/bin/sh", &_binary_sh_elf_start, size);

	if (ret)
	{
		Print ("sh failed with code %i\n", ret);
		while (1);
	}

	size = (unsigned int) &_binary_save_elf_end - (unsigned int) &_binary_save_elf_start;

	ret = UploadHelper ("/bin/save", &_binary_save_elf_start, size);

	if (ret)
	{
		Print ("sh failed with code %i\n", ret);
		while (1);
	}
}

#if VFS_FATFS_ENABLE_REFORMAT == 2
void FatfsInitUpload (void)
{
	FatfsInit ();

	struct statvfs st;
	MemSet (&st, 0, sizeof (st));

	int ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	UploadExternalFlash ();

	ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	Print ("Upload complete\n");
}

KMOD_DESCRIPTOR (fatfs, FatfsInitUpload, "FatFS");
#endif
#endif

///////////////////////////////////////////////////////////////////////////////
// Both format and upload
///////////////////////////////////////////////////////////////////////////////
#if VFS_FATFS_ENABLE_REFORMAT == 3
void FatfsInitReformatUpload (void)
{
	// Standard init
	FatfsInit ();

	// Stat the fs, allows seeing if there is anything there
	struct statvfs st;
	MemSet (&st, 0, sizeof (st));

	int ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	// Reformat, hang if failed
	if (ReformatExternalFlash ())
		while (1);

	// Stat the file system again, but hang this time if it fails
	ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	if (ret)
		while (1);

	// Upload files, hangs in the call on error
	UploadExternalFlash ();

	// Stat, but this time hang on error
	ret = vfs_fatfs_statvfs ("0:/", &st);
	PrintFileSystemStats (ret, &st);

	if (ret)
		while (1);

	// Done
	Print ("Upload complete\n");
}

KMOD_DESCRIPTOR (fatfs, FatfsInitReformatUpload, "FatFS");
#endif
