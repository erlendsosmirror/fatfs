###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = fatfs
PROJECT_ENTRY = FatfsInit
PROJECT_FILES = $(shell find src -name "*.*")
USR_INC = -Ifatfs/source -I../../kernel/kernel/inc -Iconfig/live

###############################################################################
# Compiler flags, file processing and standard targets
###############################################################################
include ../../util/build/makefile/kmod-flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets

ifeq ($(ARCH),tms570ls1224)
FILEARCH = elf32-bigarm
else
FILEARCH = elf32-littlearm
endif

###############################################################################
# Makefile execution
###############################################################################
all: build/$(PROJECT_NAME).a

build/$(PROJECT_NAME).a: $(OBJECTS)
	@arm-none-eabi-ar rcs $@ $(OBJECTS)

mkpreload: $(OBJECTS)
	@cp ../../bin/sh/build/sh.elf build/sh.elf
	@cp ../../bin/save/build/save.elf build/save.elf
	@cd build && arm-none-eabi-objcopy -I binary -O $(FILEARCH) -B ARM sh.elf sh.elf.o
	@cd build && arm-none-eabi-objcopy -I binary -O $(FILEARCH) -B ARM save.elf save.elf.o
	@arm-none-eabi-ar rcs build/$(PROJECT_NAME).a $(OBJECTS) build/sh.elf.o build/save.elf.o
