/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include "ff.h"
#include "diskio.h"
#include "fs/vfs.h"
#include "fs/dev/dev.h"
#include "print/print.h"
#include "sched/scheduler.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
int semaphores[5] = {0, 0, 0, 0, 0};
int seminuse = 0;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int ff_cre_syncobj (BYTE vol, FF_SYNC_t *sobj)
{
	if (seminuse == 5)
	{
		Print ("Error: No more semaphores\n");
		while (1);
	}
	else
	{
		SchedEnterCritical ();
		*sobj = seminuse;
		seminuse++;
		semaphores[*sobj] = 0;
		SchedExitCritical ();
		return 1;
	}
}

int ff_del_syncobj (FF_SYNC_t sobj)
{
	Print ("ff_del_syncobj\n");
	return 1;
}

int ff_req_grant (FF_SYNC_t sobj)
{
	SchedEnterCritical ();

	int idx = (int) sobj;
	int ret = 0;

	if (semaphores[idx] == 0)
	{
		semaphores[idx]++;
		ret = 1;
	}
	else
		Print ("ff_req_grant error\n");

	SchedExitCritical ();
	return ret;
}

void ff_rel_grant (FF_SYNC_t sobj)
{
	SchedEnterCritical ();

	int idx = (int) sobj;

	if (semaphores[idx] == 1)
		semaphores[idx]--;
	else
	{
		Print ("ff_rel_grant error\n");
		while (1);
	}

	SchedExitCritical ();
}

void* ff_memalloc (UINT msize)
{
	Print ("ff_memalloc not implemented\n");
	while (1);
	return 0;
}

void ff_memfree (void* mblock)
{
	Print ("ff_memfree not implemented\n");
	while (1);
}

